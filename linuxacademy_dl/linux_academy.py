# -*- coding: utf-8 -*-
#
#
# This file is a part of 'linuxacademy-dl' project.
#
# Copyright (c) 2016-2017, Vassim Shahir
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

from __future__ import unicode_literals, absolute_import, print_function
from . import __title__
from distutils.spawn import find_executable
from .url_templates import render_url, extract_auth0_form_values, parse_url_fragment
from ._session import session
from .exceptions import LinuxAcademyException
from .course import Course

from .url_templates import LOGOUT_URL, AUTH0_LOGIN_PHASE1, AUTH0_LOGIN_PHASE2, AUTH0_LOGIN_PHASE3, AUTH0_LOGIN_PHASE4

import os
import logging
import urllib

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

try:
    from urllib.parse import parse_qs
except ImportError:
    from urlparse import parse_qs

try:
    from urllib.parse import urldefrag
except ImportError:
    from urlparse import urldefrag

logger = logging.getLogger(__title__)


class DownloadParams(object):
    def __init__(self, output_dir, use_ffmpeg=False,
                 video_quality='1080'):
        self.output_dir = output_dir
        self.use_ffmpeg = use_ffmpeg
        self.video_quality = video_quality

    @property
    def output_dir(self):
        return self.__output_dir

    @output_dir.setter
    def output_dir(self, value):
        if os.path.exists(value):
            if not os.access(value, os.W_OK):
                raise LinuxAcademyException(
                    'The output directory does not have write permission.'
                )
        else:
            raise LinuxAcademyException(
                'The output directory does not exist.'
            )
        self.__output_dir = value

    @property
    def use_ffmpeg(self):
        return self.__use_ffmpeg

    @use_ffmpeg.setter
    def use_ffmpeg(self, value):
        if value and not find_executable('ffmpeg'):
            raise LinuxAcademyException(
                'ffmpeg is not found. Please install it.'
            )
        self.__use_ffmpeg = value

    @property
    def video_quality(self):
        return self.__video_quality

    @video_quality.setter
    def video_quality(self, value):
        if value not in ['1080', '720', '480', '360']:
            raise LinuxAcademyException(
                'Video quality is invalid.'
            )
        self.__video_quality = value


class LinuxAcademy(object):
    def __init__(self, course_url, username, password, output_dir,
                 use_ffmpeg=False, video_quality='1080'):

        self.username = username
        self.password = password

        self._course = Course(
            course_id=self.get_course_id(course_url),
            download_params=DownloadParams(
                output_dir,
                use_ffmpeg,
                video_quality
            )
        )

    def __enter__(self):
        self.login()
        return self

    def __exit__(self, *args):
        self.logout()

    def get_course_id(self, url):
        return int(url.rsplit("/", 1)[1])

    def login(self):
        logger.info('Logging in ...')

        #########################################
        #               PHASE 1                 #
        #########################################
        req = render_url(
            session, AUTH0_LOGIN_PHASE1
        )

        redirect_url= req.url

        if req.status_code != 200:
            raise LinuxAcademyException(
                'Error while logging in! Auth0 returned error code {}'.format(str(req.status_code)))


        ########################################
        #               PHASE 2                #
        ########################################

        # Extracts the value of the "state" parameter from the redirection url
        parsed = urlparse(redirect_url)
        state = parse_qs(parsed.query)['state']

        req = render_url(
            session, AUTH0_LOGIN_PHASE2,
            payload={
                "state": state,
                "username": self.username,
                "password": self.password
            }
        )

        if req.status_code == 401:
            raise LinuxAcademyException(
                'Username or email not found! Please try again.'
            )
        if req.status_code != 200:
            raise LinuxAcademyException(
                'Error when logging in! Auth0 returned error code {}'.format(str(req.status_code)))


        ########################################
        #               PHASE 3                #
        ########################################
        # Extracts variables from html to submit the form with the POST request
        form = extract_auth0_form_values(req.text)
        logger.info('Retrieving an authentication token. This may take a while...')
        req = render_url(
            session,  AUTH0_LOGIN_PHASE3,
            payload={
                "wa": form['wa'],
                "wresult": form['wresult'],
                "wctx": form['wctx']
            }
        )

        if req.status_code != 200:
            raise LinuxAcademyException(
                'Error while logging in! Auth0 returned error code {}'.format(str(req.status_code)))


        ########################################
        #               PHASE 4                #
        ########################################
        # Extracts the token value given via the redirect url fragment of the callback
        redirect_url = req.url
        _, fragment_string =  urldefrag(redirect_url)
        fragment = parse_url_fragment(fragment_string)
        logger.info('Authentication token acquired!')

        AUTH0_LOGIN_PHASE4['url'] = ''.join([AUTH0_LOGIN_PHASE4['url'], fragment['access_token']])
        req = render_url(
            session,  AUTH0_LOGIN_PHASE4
        )
        # There will be a redirect if the login is successful.
        # Otherwise it will be an invalid login
        if req.status_code != 200:
            raise LinuxAcademyException(
                'Error while logging in! Auth0 returned error code {}'.format(str(req.status_code)))

    def logout(self):
        render_url(session, LOGOUT_URL)
        logger.info('Logged Out!')

    def analyze(self):
        logger.info('Analyzing ...')
        self._course.assets()

    def download(self):
        logger.info('Downloading files ...')
        self._course.download()
        logger.info('Downloaded all files.')

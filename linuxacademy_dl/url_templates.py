# -*- coding: utf-8 -*-
#
#
# This file is a part of 'linuxacademy-dl' project.
#
# Copyright (c) 2016-2017, Vassim Shahir
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

from __future__ import unicode_literals, absolute_import, print_function
from string import Template
from six import text_type
from six.moves.urllib.parse import urljoin, urlencode
from bs4 import BeautifulSoup
from random import choice
import string

def random_string_generator(size=32, chars=string.ascii_uppercase + string.digits):
    return ''.join(choice(chars) for _ in range(size))

BASE_URL = 'https://linuxacademy.com'
AUTH0_CLIENT_ID = 'KaWxNn1C2Gc7n83W9OFeXltd8Utb5vvx'
AUTH0CLIENT = 'eyJuYW1lIjoiYXV0aDAuanMiLCJ2ZXJzaW9uIjoiOC4xMC4xIn0='
AUTH0_URL = 'https://lacausers.auth0.com'
NONCE = random_string_generator()


USER_AGENT = 'Mozilla/5.0 ' \
            '(X11; Ubuntu; Linux x86_64; rv:50.0) ' \
            'Gecko/20100101 Firefox/50.0'

# Initiates a session with Auth0
AUTH0_LOGIN_PHASE1 = {
    'url': urljoin(AUTH0_URL, '/authorize?{}'.format(urlencode({
        'client_id': AUTH0_CLIENT_ID,
        'response_type': 'token id_token',
        'redirect_uri': BASE_URL,
        'scope': 'openid email user_impersonation profile',
        'audience': 'https://linuxacademy.com',
        'nonce': NONCE,
        'auth0Client': AUTH0CLIENT,
        'state': random_string_generator()
     }))),
    'method': 'GET',
    "allow_redirects": True,
}

# Sents user authentication
AUTH0_LOGIN_PHASE2 = {
    'url': urljoin(AUTH0_URL, '/usernamepassword/login'),
    'method': 'POST',
    "allow_redirects": True,
    'data': {
        'client_id': AUTH0_CLIENT_ID,
        'response_type': 'token id_token',
        'tenant': 'lacausers',
        'protocol': 'oauth2',
        'connection': 'Username-Password-Authentication',
        'redirect_uri': urljoin(BASE_URL, '/cp/ssologin'),
        'scope': 'openid email user_impersonation profile',
        'audience': 'https://linuxacademy.com',
        'nonce': NONCE,
        'auth0Client': AUTH0CLIENT,
        'username': None,
        'password': None,
        'sso': 'true',
        'state': None,
        '_csrf': 'deprecated'
    }
}

# Submits the form from previous response to Auth0 for
# the completion of the authentication request and return a token
# (a final redirect to linuxacademy callback URL)
AUTH0_LOGIN_PHASE3 = {
    'url': urljoin(AUTH0_URL, '/login/callback'),
    'method': 'POST',
    "allow_redirects": True,
    'data': {
        'wa': None,
        'wresult': None,
        'wctx': None
    }
}

# Sends the token to linuxacademy to authenticate the Session
AUTH0_LOGIN_PHASE4 = {
    'url': urljoin(BASE_URL, '/cp/login/tokenValidateLogin/token/'),
    'method': 'GET',
    "allow_redirects": True,
}


LOGOUT_URL = {
    'url': urljoin(BASE_URL, 'cp/login/quit'),
    'method': 'GET',
    'headers': {
        'Referer': urljoin(BASE_URL, '/cp/login')
    },
    "allow_redirects": False
}

COURSE_URL = {
    'url': urljoin(BASE_URL, '/cp/modules/view/id/$course_id')
}

COURSE_LIST = {
    'url': urljoin(BASE_URL, '/cp/socialize/course_module_search'),
    'method': 'GET',
    'headers': {
        'Referer': urljoin(BASE_URL, '/cp/login'),
        'X-Requested-With': 'XMLHttpRequest'
    }
}

COURSE_SYLLABUS = {
    'url': urljoin(BASE_URL, '/cp/modules/syllabus/id/$course_id'),
    'method': 'GET',
    'headers': {
        'Referer': urljoin(BASE_URL, '/cp/modules/view/id/$course_id'),
        'X-Requested-With': 'XMLHttpRequest'
    }
}


def extract_auth0_form_values(html_content):
   result = {}
   soup = BeautifulSoup(html_content, 'html.parser')
   rows = soup.findAll('input')

   for input in rows:
     result[input.get('name')] = input.get('value')
   return result

def parse_url_fragment(fragment_string):
    fragment = {}
    fragment_item_strings = fragment_string.split('&')

    for i in range(len(fragment_item_strings)):
         fragment_item = fragment_item_strings[i].split('=')
         fragment[fragment_item[0]] = fragment_item[1]
    return fragment

def render_url(session, url_template,
               template_params={"base_url": BASE_URL}, payload={}):
    def render(source):
        for k, v in source.items():
            if isinstance(v, dict):
                render(v)
            elif isinstance(v, text_type):
                source[k] = Template(v).safe_substitute(template_params)
            elif v is None:
                source[k] = payload.get(k, None)
        return source
    return  getattr(session, 'request')(**render(dict(url_template)))
